<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('images/logo.png') }}" type="image/x-icon">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
</head>
<body>
    <div id="app">
        <div class="wrapper">
            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                    </li>
                </ul>

                <ul class="navbar-nav ml-auto">
                    @if($data['main'] == null)
                        <li>
                            <a href="{{ route('dashboard') }}"> Dashboard&nbsp;</a> <i class="fas fa-arrow-right"></i>
                        </li>
                        @if($data['previousRoute'] != null)
                            <li>
                                <a href="{{ route($data['previousRoute']) }}">&nbsp;{{ $data['previousPage'] }}</a>
                                <i class="fas fa-arrow-right"></i>
                            </li>
                        @endif
                        @if($data['route'] != null)
                            <li> <a href="{{ route($data['route']) }}">&nbsp;{{ $data['page'] }}</a></li>
                        @else
                            <li>&nbsp;{{ $data['page'] }}</li>
                        @endif
                    @else
                        <li>&nbsp;{{ $data['main'] }}</li>
                    @endif
                </ul>
            </nav>
            <!-- /.navbar -->

            <!-- Main Sidebar Container -->
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <!-- Brand Logo -->
                <a href="" class="brand-link">
                <img src="{{ URL::to('/images/logo.png') }}" alt="" class="brand-image img-circle elevation-3"
                    style="opacity: .8">
                    <span class="brand-text font-weight-light">CyberTech Task</span>
                </a>

                <!-- Sidebar -->
                <div class="sidebar">
                    <!-- Sidebar user panel (optional) -->
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div class="image">
                            <img src="{{ URL::to('/images/avatar.png') }}" class="img-circle elevation-2" alt="">
                        </div>
                        <div class="info">
                            <a href="" class="d-block">{{ auth()->user()->name }}</a>
                        </div>
                    </div>

                    <!-- Sidebar Menu -->
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                            <li class="side__menu">
                                <span class="text-light"> General</span>
                            </li>

                            <li class="nav-item">
                                <a href="{{ route('dashboard') }}" class="nav-link {{ shortHandIfOptional(Request::segment(1), 'dashboard', 'active', '') }}">
                                    <i class="nav-icon fas fa-tachometer-alt"></i>
                                    <p> Dashboard</p>
                                </a>
                            </li>

                            <li class="nav-item has-treeview
                            {{ shortHandIfOptional(Request::segment(1), 'emailmanager', 'menu-open', '') }}
                            {{ shortHandIfOptional(Request::segment(1), 'emailschedular', 'menu-open', '') }}">

                                <a href="#" class="nav-link
                                {{ shortHandIfOptional(Request::segment(1), 'emailmanager', 'active', '') }}
                                {{ shortHandIfOptional(Request::segment(1), 'emailschedular', 'active', '') }}">
                                    <i class="nav-icon las la-envelope"></i>
                                    <p> Email Management <i class="right fas fa-angle-left"></i> </p>
                                </a>
                                <ul class="nav nav-treeview ml-1">
                                    <li class="nav-item">
                                        <a href="/emailmanager" class="nav-link {{ shortHandIfOptional(Request::segment(1), 'emailmanager', 'active', '') }}">
                                            <i class="nav-icon las la-envelope"></i>
                                            <p>Email Manager </p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="/emailschedular" class="nav-link {{ shortHandIfOptional(Request::segment(1), 'emailschedular', 'active', '') }}">
                                            <i class="nav-icon las la-clock"></i>
                                            <p>Email Scheduler </p>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item">
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();" class="nav-link">
                                    <i class="nav-icon fas fa-sign-out-alt"></i>
                                    <p> {{ __('Sign Out') }} </p>
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </nav>
                </div>
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <div class="content">
                    <main class="py-2">
                        <div class="container-fluid">
                        </div>
                        @yield('content')
                    </main>
                </div>
            </div>

            <!-- Main Footer -->
            <footer class="main-footer">
                <!-- To the right -->
                <div class="float-right d-none d-sm-inline">
                {{-- Anything you want --}}
                </div>
                <!-- Default to the left -->
                <strong>Copyright &copy; {{ now()->year }} <a href="" target="_blank">Company</a>.</strong> All rights reserved.
            </footer>
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="//cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    @yield('script')
</body>
</html>

@php
    $data=[
        'main' => 'Dashboard',
        'page' => null,
        'previousPage' => null,
        'previousRoute' => null,
        'breadcumb' => null,
        'route' => null
    ]
@endphp

@extends('layouts.app', $data)
@section('title', 'Dashboard')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h4>Welcome to Bulk Email Management System !</h4>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

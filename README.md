# Configuration settings

## Do the following changes in .env file

* DB_DATABASE=task_db
* DB_USERNAME=your_mysql_db_user_name
* DB_PASSWORD=your_mysql_db_password


* MAIL_FROM_ADDRESS=email_which_you_want_to_send_from
* MAIL_PASSWORD=email_password_you_have_provided


## Login

* You can have your own login using system register Or
* Use this login
* *  Email: developer@gmail.com 
* *  Password: 1234 

## Steps

* Email Manager Section
* * Download the Excel File and fill the details.
* * Upload the excel File.


* Email Scheduler Section
* * Select the batch using dropdown. It will select the all emails belongs to the selected batch
* * Create the email subject and body, then click submit button

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    use HasFactory;

    protected $table = 'people';

    public function batch() {
        return $this->belongsTo('App\Models\Batch', 'batch_id');
    }
    
    public function message() {
        return $this->belongsTo('App\Models\Message', 'message_id');
    }
    
    public function person_messages() {
        return $this->hasMany('App\Models\MessagePerson');
    }

    protected $fillable = [
        'batch_id',
        'name',
        'phone',
        'email',
    ];
}

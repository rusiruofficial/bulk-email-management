<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;
    
    public function person() {
        return $this->belongsTo('App\Models\Person', 'person_id');
    }
    public function person_messages() {
        return $this->hasMany('App\Models\MessagePerson');
    }
    public function batch() {
        return $this->belongsTo('App\Models\Batch', 'batch_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessagePerson extends Model
{
    use HasFactory;
    protected $table = 'message_people';

    public function person() {
        return $this->belongsTo('App\Models\Person', 'person_id');
    }
    public function message() {
        return $this->belongsTo('App\Models\Message', 'message_id');
    }
    public function batch() {
        return $this->belongsTo('App\Models\Batch', 'batch_id');
    }
}

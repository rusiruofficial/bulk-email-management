<?php

namespace App\Imports;

use App\Models\Batch;
use App\Models\Person;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Validators\Failure;

class PersonImport implements ToCollection, WithValidation, SkipsOnFailure
{
    public function  __construct($batch_id)
    {
        $this->batch_id= $batch_id;
    }

    public function rules(): array
    {
        return [
            '0' => 'required|string',
            '1' => 'required|string',
            '2' => 'required|string',
        ];
    }

    public function onFailure(Failure ...$failures)
    {
        // Handle the failures how you'd like.
    }

    public function collection(Collection $collection)
    {
        // dd($this->batch_id);
        foreach ($collection as $key => $value) {
            if ($key > 0) {
                if($value->filter()->isNotEmpty()){
                    Person::create([
                        'batch_id'     => $this->batch_id,
                        'name'     => $value[0],
                        'phone'    => $value[1],
                        'email' => $value[2],
                    ]);
                }
            }
        }
    }
}

<?php


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


function generateBatch($string) {
    $output = strtoupper(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string)));
    $last = substr($output, -1);

    if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $last)) {
        $output = strtoupper(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', substr_replace($output, '', -1))));
    }

    return $output;
}

function shortHandIfOptional($value, $check, $true, $false)
{
    return ($value == $check) ? $true : $false;
}

function shortHandIf($value, $true, $false)
{
    return $value ? $true : $false;
}

function composeEmail($to, $subject, $body, $file) {
    require base_path("vendor/autoload.php");
    $mail = new PHPMailer(true);

    // dd($file);
    try {
        $mail->SMTPDebug = 0;
        $mail->isSMTP();
        $mail->Host = 'smtp.gmail.com';             //  smtp host
        $mail->SMTPAuth = true;
        $mail->Username = env('MAIL_FROM_ADDRESS');   //  sender username
        $mail->Password = env('MAIL_PASSWORD');       // sender password
        $mail->SMTPSecure = 'tls';                  // encryption - ssl/tls
        $mail->Port = 587;                          // port - 587/465

        $mail->setFrom(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
        $mail->addAddress($to);

        if($file != null) {
            for ($i=0; $i < count($_FILES[$file]['tmp_name']); $i++) {
                $mail->addAttachment($_FILES[$file]['tmp_name'][$i], $_FILES[$file]['name'][$i]);
            }
        }

        $mail->isHTML(true);                // Set email content format to HTML

        $mail->Subject = $subject;
        $mail->Body    = $body;

        if(!$mail->send() ) {
            return back()->with("failed", "Email not sent.")->withErrors($mail->ErrorInfo);
        } else {
            return back()->with("success", "Email has been sent.");
        }

    } catch (Exception $e) {
        return back()->with('error','Message could not be sent.');
    }
}

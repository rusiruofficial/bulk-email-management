<?php

namespace App\Console\Commands;

use App\Models\Batch;
use App\Models\Message;
use App\Models\MessagePerson;
use App\Models\Person;
use Carbon\Carbon;
use Illuminate\Console\Command;

class EmailCheck extends Command
{
    protected $signature = 'mail:send';

    protected $description = 'This command gonna send daily emails';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $now = Carbon::now()->format('m-d-Y');

        $data = MessagePerson::with(['message', 'person'])->where('status', false)->get();

        foreach ($data as $key => $person) {
            if($now == $person['message']->date) {
                composeEmail($person['person']->email, $person['message']->subject, $person['message']->body, $person['message']->attachment);

                MessagePerson::whereIn('id', [$person->id])->update(['status' => true]);
                Message::whereIn('id', [$person->message_id])->update(['status' => true]);
                Person::whereIn('id', [$person->person_id])->update(['status' => true]);
                Batch::whereIn('id', [Person::whereIn('id', [$person->person_id])->get()])->update(['status' => true]);
            }
        }
    }
}

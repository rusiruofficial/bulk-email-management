@php
    $data=[
        'main' => null,
        'page' => 'Email Scheduler',
        'previousPage' => null,
        'previousRoute' => null,
        'breadcumb' => null,
        'route' => null
    ]
@endphp

@extends('emailschedular::layouts.master', $data)
@section('title', 'Email Schedular')

@section('content')
<div class="container-fluid">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><strong>Bulk Emails</strong></h3>

            <a href="{{ route('create') }}" class="float-right btn btn-sm btn-success">Create <i class="fas fa-plus"></i></a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="card-body table-responsive">
                <table class="table table-hover" id="datatable">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Person</th>
                            <th scope="col">Email</th>
                            <th scope="col">Subject</th>
                            <th scope="col">Body</th>
                            <th scope="col">Date</th>
                            <th scope="col">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($messages as $message)
                            <tr>
                                <th>{!! $message->id !!}</th>
                                <td>{!! $message->person->name !!}</td>
                                <td>{!! $message->person->email !!}</td>
                                <td>{!! Str::substr($message->message->subject, 0, 20) !!}</td>
                                <td>{!! Str::substr($message->message->body, 0, 40)  !!}</td>
                                <td>{!! $message->message->date !!}</td>
                                {{-- <td>
                                    <form action="{{ route('destroy', $message->id) }}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-sm btn-danger"> <i class="fa fa-trash-alt"></i> </button>
                                    </form>
                                </td> --}}
                                <td>
                                    @if($message->status == true)
                                        <span class="badge badge-info text-light">Sent</span>
                                    @else
                                        <span class="badge badge-dark">Pending</span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready( function () {

        var datatable = $('#datatable').DataTable({
            lengthMenu: [
                [10, 15, 25, 50, 100, 125, 150, -1],
                [10, 15, 25, 50, 100, 125, 150, "All"]
            ],
            columnDefs: [{
                orderable: true,
                className: 'select-checkbox',
                targets: 0
            }],
            select: {
                style: 'os',
                selector: 'td:first-child'
            },
            order: [
                [1, 'asc']
            ],
            paging:true,
            lengthChange: true,
            ordering: true,
            searching: true,
            autoWidth: true,
            bFilter: true, //hide Search bar
            bInfo: true, // hide showing entries
            stateSave: true,
        });

        $("#bulkEmailForm").validate();

        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0');
        var yyyy = today.getFullYear();

        today = mm + '-' + dd + '-' + yyyy;
        $("#date").val(today);

        $('#date').duDatepicker( {
            format: 'mm-dd-yyyy',
            minDate: today
        });
    } );



    // $("#bulkEmailForm").submit(function (e) {
    //     e.preventDefault();
    //     tinyMCE.triggerSave();
    //     var formValues= $(this).serialize();
    //     var action = $(this).attr('action');

    //     $.ajax({
    //         data: formValues,
    //         url: action,
    //         type: 'POST',
    //         success: function(response) {
    //             location.reload(true);
    //         },
    //         error: function (xhr, desc, err){
    //             // $('#emailAlert').text(xhr.responseJSON.errors.email[0])
    //         },
    //     });
    // });

</script>
@endsection

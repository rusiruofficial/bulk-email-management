@php
    $data=[
        'main' => null,
        'page' => 'Create Schedule Email',
        'previousPage' => 'Bulk Emails',
        'previousRoute' => 'index',
        'breadcumb' => null,
        'route' => null
    ]
@endphp

@extends('emailschedular::layouts.master', $data)
@section('title', 'Create Schedula Email')

@section('content')
<div class="container-fluid">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><strong>Create Bulk Emails</strong></h3>

            <a href="{{ route('index') }}" class="float-right btn btn-sm btn-info">View <i class="fas fa-eye"></i></a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <form action="{{ route('store') }}" method="POST" enctype="multipart/form-data" id="bulkEmailForm">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Select The Batch <span class="text-danger"> * </span></label>
                            <select class="form-control selectpicker" name="batch_id[]" id="batch_id" data-live-search="true" title="Choose a batch" data-size="5" multiple required data-actions-box="true">
                                @foreach ($batches as $batch)
                                    <option value="{{ $batch->code }}"> {{ $batch->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Schedule Date <span class="text-danger"> * </span></label>
                            <input type="text" required name="date" id="date" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Subject <span class="text-danger"> * </span></label>
                            <input type="text" required name="subject" id="subject" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Attachments</label>
                            <input type="file" name="attachment" id="attachment" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Body <span class="text-danger"> * </span></label>
                            <textarea name="body" required id="body" class="form-control" cols="30" rows="10"></textarea>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                            <button type="reset" class="btn btn-sm btn-dark"> Cancel <i class="fas fa-redo-alt"></i> </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready( function () {

        $("#bulkEmailForm").validate();

        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0');
        var yyyy = today.getFullYear();

        today = mm + '-' + dd + '-' + yyyy;
        $("#date").val(today);

        $('#date').duDatepicker( {
            format: 'mm-dd-yyyy',
            minDate: today
        });
    } );

    // $("#bulkEmailForm").submit(function (e) {
    //     e.preventDefault();
    //     tinyMCE.triggerSave();
    //     var formValues= $(this).serialize();
    //     var action = $(this).attr('action');

    //     $.ajax({
    //         data: formValues,
    //         url: action,
    //         type: 'POST',
    //         success: function(response) {
    //             location.reload(true);
    //         },
    //         error: function (xhr, desc, err){
    //             // $('#emailAlert').text(xhr.responseJSON.errors.email[0])
    //         },
    //     });
    // });

</script>
@endsection

<?php

namespace Modules\EmailSchedular\Http\Controllers;

use App\Models\Batch;
use App\Models\Message;
use App\Models\MessagePerson;
use App\Models\Person;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class EmailSchedularController extends Controller
{
    public function index()
    {
        $messages = MessagePerson::with(['person', 'message', 'batch'])->orderBy('id', 'asc')->get();
        // dd($messages);
        return view('emailschedular::index')->with([
            'messages' => $messages,
        ]);
    }

    public function create()
    {
        $batches = Batch::with('persons')->where('status', false)->orderBy('name', 'asc')->get();
        return view('emailschedular::create')->with([
            'batches' => $batches,
        ]);
    }

    public function store(Request $request)
    {
        $now = Carbon::now()->format('m-d-Y');

        $batch_ids = $request->batch_id;

        $persons = Person::with('batch')->whereHas('batch', function ($query) use($batch_ids) {
            $query->whereIn('code', $batch_ids);
        })->get();

        if ($now == $request->date) {
            foreach ($persons as $key => $person) {
                composeEmail($person->email, $request->subject, $request->body, $request->attachment);
            }
        } else {
            $message = new Message;
            $message->subject = $request->subject;
            $message->body = $request->body;
            $message->attachment = $request->attachment;
            $message->date = $request->date;
            $message->status = false;
            $message->save();

            foreach ($persons as $key => $person) {
                $messagePerson = new MessagePerson;
                $messagePerson->message_id = $message->id;
                $messagePerson->person_id = $person->id;
                $messagePerson->save();
            }
        }

        return back();
    }

}

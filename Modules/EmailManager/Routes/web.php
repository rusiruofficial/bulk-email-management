<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::prefix('emailmanager')->group(function() {
    Route::get('/', 'EmailManagerController@index')->name('index');
    Route::get('/create', 'EmailManagerController@create')->name('create');
    Route::post('/', 'EmailManagerController@store')->name('store');
    Route::delete('/{id}', 'EmailManagerController@destroy')->name('destroy');
});

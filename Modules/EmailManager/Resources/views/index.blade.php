@php
$data = [
    'main' => null,
    'page' => 'Email Manager',
    'previousPage' => null,
    'previousRoute' => null,
    'breadcumb' => null,
    'route' => null,
];
@endphp

@extends('emailmanager::layouts.master', $data)
@section('title', 'Email Manager')

@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">List</h3>

                <a href="{{ URL::to('/emailmanager/create') }}" class="float-right btn btn-sm btn-success">Upload <i class="fas fa-upload"></i></a>

                {{-- <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div> --}}
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="card-body table-responsive">
                    <table class="table table-hover" id="datatable">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Batch Name</th>
                                <th scope="col">Batch Code</th>
                                <th scope="col">Person</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Email</th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($persons as $person)
                                <tr>
                                    <th>{!! $person->id !!}</th>
                                    <td>{!! $person->batch->name !!}</td>
                                    <td>{!! $person->batch->code !!}</td>
                                    <td>{!! $person->name !!}</td>
                                    <td>{!! $person->phone !!}</td>
                                    <td>{!! $person->email !!}</td>

                                    <td>
                                        @if($person->status == true)
                                            <span class="badge badge-info text-light">Sent</span>
                                        @else
                                            <span class="badge badge-dark">Pending</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script>


$(document).ready( function () {
    $("#fileUploadForm").validate();

    var datatable = $('#datatable').DataTable({
        lengthMenu: [
            [10, 15, 25, 50, 100, 125, 150, -1],
            [10, 15, 25, 50, 100, 125, 150, "All"]
        ],
        columnDefs: [{
            orderable: true,
            className: 'select-checkbox',
            targets: 0
        }],
        select: {
            style: 'os',
            selector: 'td:first-child'
        },
        order: [
            [1, 'asc']
        ],
        paging:true,
        lengthChange: true,
        ordering: true,
        searching: true,
        autoWidth: true,
        bFilter: true, //hide Search bar
        bInfo: true, // hide showing entries
        stateSave: true,
    });
} );


// $("#loginForm").submit(function (e) {
//     e.preventDefault();
//     var formValues= $(this).serialize();
//     var action = $(this).attr('action');

//     $.ajax({
//         data: formValues,
//         url: action,
//         type: 'POST',
//         success: function(response) {
//             location.reload(true);
//         },
//         error: function (xhr, desc, err){
//             $('#emailAlert').text(xhr.responseJSON.errors.email[0])
//         },
//     });
// });
</script>
@endsection

@php
$data = [
    'main' => null,
    'page' => 'Upload Excels',
    'previousPage' => 'Email Manager',
    'previousRoute' => 'index',
    'breadcumb' => null,
    'route' => null,
];
@endphp

@extends('emailmanager::layouts.master', $data)
@section('title', 'Upload Excels')

@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><strong>Step 01 -</strong> Excel file download</h3>

                <a href="{{ URL::to('/emailmanager') }}" class="float-right btn btn-sm btn-info">View <i class="fas fa-eye"></i></a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Please fill your details with Name,Number, email address</label>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <a title="Download The Excel Sheet" href="{{ asset('download/CyberTechDetailsSheet.xlsx') }}" download="CyberTechDetailsSheet" class="btn btn-sm btn-dark">Download <i class="fas fa-download"></i></a>
                        </div>
                        <!-- /.form-group -->
                    </div>
                </div>
                <!-- /.row -->
            </div>
        </div>


        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><strong>Step 02 -</strong> Excel file upload</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form action="/emailmanager" method="POST" enctype="multipart/form-data" id="fileUploadForm">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Select the download excel file</label>
                                <input type="file" name="file" class="form-control" required accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="">Batch Name</label>
                                <input type="text" name="name" class="form-control" required placeholder="Please enter Batch name">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label for="">Batch Code</label>
                                <input type="text" name="code" class="form-control" required placeholder="Please enter Batch code">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-sm btn-primary"> Upload <i class="fas fa-upload"></i> </button>
                                <button type="reset" class="btn btn-sm btn-dark"> Cancel <i class="fas fa-redo-alt"></i> </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script>


$(document).ready( function () {
    $("#fileUploadForm").validate();
} );

// $("#fileUploadForm").submit(function (e) {
//     e.preventDefault();
//     var formValues= $(this).serialize();
//     var action = $(this).attr('action');

//     $.ajax({
//         data: formValues,
//         url: action,
//         type: 'POST',
//         success: function(response) {
//             location.reload(true);
//         },
//         error: function (xhr, desc, err){
//             // $('#emailAlert').text(xhr.responseJSON.errors.email[0])
//         },
//     });
// });
</script>
@endsection

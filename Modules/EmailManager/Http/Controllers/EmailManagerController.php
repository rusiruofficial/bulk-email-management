<?php

namespace Modules\EmailManager\Http\Controllers;

use App\Imports\PersonImport;
use App\Models\Batch;
use App\Models\Person;
use App\Models\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Maatwebsite\Excel\Facades\Excel;


class EmailManagerController extends Controller
{
    public function index()
    {
        $persons = Person::all();
        return view('emailmanager::index')->with([
            'persons' => $persons,
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:xls,xlsx',
            'name' => 'required',
            'code' => 'required'
        ]);

        $path = $request->file('file')->getRealPath();

        $batch = new Batch();
        $batch->name = $request->name;
        $batch->code = generateBatch($request->code);
        $batch->status = false;
        $batch->save();

        // dd($batch->id);

        Excel::import(new PersonImport($batch->id), $path);
        return back();
    }

    public function create()
    {
        return view('emailmanager::create');
    }


    public function show($id)
    {
        return view('emailmanager::show');
    }

    public function edit($id)
    {
        return view('emailmanager::edit');
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $person = Person::find($id);
        $person->delete();
        return back();
    }
}
